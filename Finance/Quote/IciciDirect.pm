#
#    Copyright (C) 1998, Dj Padzensky <djpadz@padz.net>
#    Copyright (C) 1998, 1999 Linas Vepstas <linas@linas.org>
#    Copyright (C) 2000, Yannick LE NY <y-le-ny@ifrance.com>
#    Copyright (C) 2000, Paul Fenwick <pjf@cpan.org>
#    Copyright (C) 2000, Brent Neal <brentn@users.sourceforge.net>
#    Copyright (C) 2013, Manoj Kumar
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
#    02111-1307, USA
#
# This code derived from Padzensky's work on package Finance::YahooQuote,
# but extends its capabilites to encompas a greater number of data sources.
#
# This code was developed as part of GnuCash <http://www.gnucash.org/>

package Finance::Quote::IciciDirect;
require 5.005;

use strict;
use warnings;
use HTML::TreeBuilder;
use Finance::Quote::SymbolTranslation qw(symbol_translation);
use vars qw($VERSION);

$VERSION = '1.18';

#my $URL_ORIG = 'http://content.icicidirect.com/newsiteContent/includes/nsebsequotes.asp?icicicode=';
my $URL_ORIG = 'http://content.icicidirect.com/idirectcontent/basemasterpage/ContentDataHandler.ashx?pgname=getquote&ismethodcall=0&frmName=frmtiny&icode=';
my $URL = $URL_ORIG;
my $debug = 0;

sub methods {
  return (
    icici_direct => \&icici_direct,
    nse_india => \&icici_direct_nse,
    bse_india => \&icici_direct_bse,
    india_shares => \&icici_direct
  );
}

{
  my @labels = qw/currency date isodate last method success symbol exchange code/;
  sub labels {
    return (
      icici_direct => \@labels,
      nse_india => \@labels,
      bse_india => \@labels,
      india_shares => \@labels
    );
  }
}

sub icici_direct {
  icici_direct_common(@_, 'icici_direct');
}

sub icici_direct_nse {
  icici_direct_common(@_, 'icici_direct_nse');
}

sub icici_direct_bse {
  icici_direct_common(@_, 'icici_direct_bse');
}

sub icici_direct_common {
  my $quoter = shift;
  my $method = pop;
  my @symbols = @_;
  $debug = 0;
  
  if (@symbols && $symbols[0] =~ m/^Debug$|^Debug=(\d+)$/) { # if first symbol is Debug, enable debugging
    if (defined $1) {
      $debug = $1;
    }
    else {
      $debug = 1;
    }
    shift @symbols;
  }
  
  return unless @symbols;
  
  my (%info, $ref_trans_1);
  my ($ua, $url, $response);
  my ($nse, $bse);
  
  $ua = $quoter->user_agent;
  
  $nse = ($method eq 'icici_direct_bse') ? 0 : 1;
  $bse = ($method eq 'icici_direct_nse') ? 0 : 1;
  
  ($ref_trans_1, undef) = symbol_translation('icici_direct', \@symbols, 1);
  
  foreach my $symbol (@symbols) {
    
    $url = $URL . $ref_trans_1->{uc($symbol)};
    if ($debug & 1) {
      print "URL = $url\n";
    }
    
    $info{$symbol, 'method'} = 'icici_direct';
    $info{$symbol, 'success'} = 0;
    
    my $response = $ua->get($url);
    if ($response->is_success) {
      if ($debug & 16) {
        print "Content:\n";
        print "=====\n";
        print $response->decoded_content;
        print "\n=====\n\n";
      }
      
      my $root = HTML::TreeBuilder->new();
      
      $root->utf8_mode(1); # avoid warning Parsing of undecoded UTF-8 will give garbage when decoding entities at
      
      $root->parse_content($response->content);
      if ($debug & 2) {
        print "Tree:\n";
        print "=====\n";
        $root->dump();
        print "\n=====\n\n";
      }
      
      my %t_info = extract_icici_direct_info($root, $nse, $bse);
      
      if (exists $t_info{last} && exists $t_info{date}) {
        $info{$symbol, 'symbol'} = $ref_trans_1->{uc($symbol)};
        $info{$symbol, 'currency'} = 'INR';
        $info{$symbol, 'last'} = $t_info{last};
        $info{$symbol, 'exchange'} = $t_info{exchange};
        $info{$symbol, 'code'} = $t_info{code};
        $quoter->store_date(\%info, $symbol, {eurodate => $t_info{date}});
        $info{$symbol, 'success'} = 1;
      }
      else {
        $info{$symbol, 'errormsg'} = 'Stock lookup failed';
      }
      
      $root->delete();
    }
    else {
      $info{$symbol, 'errormsg'} = "HTTP failure : " . $response->status_line;
    }
  }
  
  return wantarray ? %info : \%info;
}


sub extract_info {
  my $root = shift;
  my @search = (
    {search => '^\s*Date\s*&\s*Time\s*$', label => 'date'},
    {search => '^\s*Current\s*$', label => 'last'},
    {search => '^\s*Open\s*$', label => 'open'},
    {search => '^\s*Change\s*$', label => 'net'},
    {search => '^\s*High\s*$', label => 'high'},
    {search => '^\s*%\s*Change\s*$', label => 'p_change'},
    {search => '^\s*Low\s*$', label => 'low'},
    {search => '^\s*Prev\s*Close\s*$', label => 'close'},
    {search => '^\s*52\s*Week\s*high\s*$', label => 'y_high'},
    {search => '^\s*Volume\s*$', label => 'volume'},
    {search => '^\s*52\s*Week\s*Low\s*$', label => 'y_low'},
    {search => '^\s*(NSE|BSE)\s*Code\s*$', label => 'code'},
  );
  my %info;
  my $element;
  
  if ($debug & 4) {
    print "icici_direct: extract_info:\n";
    $root->dump();
    print "\n";
  }
  
  if ($debug & 8) {
    print "icici_direct: extract_info:\n";
    foreach my $tr ($root->look_down('_tag', 'tr')) {
      foreach my $td ($tr->look_down('_tag', qr/th|td/)) {
        print $td->as_text(), "\t";
      }
      print "\n";
    }
    print "\n";
  }
  
  foreach (@search) {
    my $search = $_->{search};
    $element = $root->look_down('_tag', 'td',
      sub {
        $_[0]->as_text() =~ m/$search/i;
      }
    );
    
    if ($element && $element->right()) {
      $info{$_->{label}} = $element->right()->as_text();
      $info{$_->{label}} =~ s/^\s+//;
      $info{$_->{label}} =~ s/\s+$//;
    }
  }
  
  if (exists $info{date} && $info{date} =~ m/(\S+)\s+(\S+)/) {
    $info{date} = $1;
    $info{time} = $2;
  }

  foreach ('close', 'high', 'last', 'low', 'net', 'open', 'p_change', 'volume', 'y_high', 'y_low') {
    if (exists $info{$_}) {
      $info{$_} =~ s/,//g;
    }
  }
  
  return(%info);
}

sub data_valid {
  my $ref = shift;
  
  if (exists $ref->{'last'} && $ref->{'last'} =~ m/\d+(\.\d+)?/) {
  }
  else {
    return(0);
  }
  
  if (exists $ref->{'date'} && $ref->{'date'} =~ m/(\d+)-(\w+)-(\d+)/) {
  }
  else {
    return(0);
  }
  
  return(1);
}

sub cmp_date {
  my $date1 = shift;
  my $date2 = shift;
  
  my ($day1, $month1, $year1, $day2, $month2, $year2);
  my %months = (jan => 1, feb => 2, mar => 3, apr => 4, may => 5, jun => 6, jul => 7, aug => 8, sep => 9, oct =>10, nov =>11, dec =>12);
  
  if ($date1 =~ m/(\d+)-(\w+)-(\d+)/) {
    ($day1, $month1, $year1) = ($1, $2, $3);
    if ($month1 !~ m/\d/) {
      $month1 = $months{lc($month1)};
    }
  }
  
  if ($date1 =~ m/(\d+)-(\w+)-(\d+)/) {
    ($day2, $month2, $year2) = ($1, $2, $3);
    if ($month2 !~ m/\d/) {
      $month2 = $months{lc($month2)};
    }
  }
  
  if ($year1 > $year2) {
    return(1);
  }
  elsif ($year1 < $year2) {
    return(-1);
  }
  elsif ($month1 > $month2) {
    return(1);
  }
  elsif ($month1 < $month2) {
    return(-1);
  }
  elsif ($day1 > $day2) {
    return(1);
  }
  elsif ($day1 < $day2) {
    return(-1);
  }
  else {
    return(0);
  }
}

sub extract_icici_direct_info {
  my $root = shift;
  my $nse = shift;
  my $bse = shift;
  my ($element, %info_nse, %info_bse);
  
  $element = $root->look_down('_tag', 'div',
    sub {
      $_[0]->as_text() =~ m/^\s*NSE\s*$/;
    }
  );
  
  if ($element && $element->parent() && $element->parent()->parent() && $element->parent()->parent()->parent()) {
    %info_nse = extract_info($element->parent()->parent()->parent());
    $info_nse{'exchange'} = 'NSE';
  }
  
  if (! data_valid(\%info_nse)) {
    $nse = 0;
  }
  
  $element = $root->look_down('_tag', 'div',
    sub {
      $_[0]->as_text() =~ m/^\s*BSE\s*$/;
    }
  );
  
  if ($element && $element->parent() && $element->parent()->parent() && $element->parent()->parent()->parent()) {
    %info_bse = extract_info($element->parent()->parent()->parent());
    $info_bse{'exchange'} = 'BSE';
  }
  
  if (! data_valid(\%info_bse)) {
    $bse = 0;
  }
  
  if ($nse && $bse) {
    my $cmp = cmp_date($info_nse{'date'}, $info_bse{'date'});
    if ($cmp > 0) {
      return(%info_nse);
    }
    elsif ($cmp < 0) {
      return(%info_bse);
    }
    elsif ($info_nse{'last'} >= $info_bse{'last'}) {
      return(%info_nse);
    }
    else {
      return(%info_bse);
    }
  }
  elsif ($nse) {
    return(%info_nse);
  }
  elsif ($bse) {
    return(%info_bse);
  }
  else {
    return();
  }
}


sub url {
  if (! @_) {
    return($URL);
  }
  
  if ($_[0] eq '') {
    $URL = $URL_ORIG;
  }
  else {
    $URL = shift;
  }
}


1;

=head1 NAME

Finance::Quote::IciciDirect - Fetch quotes from ICICI Direct http://www.icicidirect.com

=head1 SYNOPSIS

    use Finance::Quote;

    $q = Finance::Quote->new();

    %info = $q->fetch('india_shares', 'TCS'); # Failover to other methods ok.
    %info = $q->fetch('nse_india', 'TCS'); # Failover to other methods ok for NSE shares.
    %info = $q->fetch('bse_india', 'TCS'); # Failover to other methods ok for BSE shares.
    %info = $q->fetch('icici_direct', 'TCS'); # Use this module only.

=head1 DESCRIPTION

This module obtains information about india shares from ICICI Direct web site http://www.icicidirect.com.

This module uses Finance::Quote::SymbolTranslation for symbol translation. Source name for this purpose is icici_direct.

=head1 SYMBOL TRANSLATION

If failover method india_shares is used with NseIndia and IciciDirect as 
sources, the same symbol can not be used with both these providers.

In order to use the same symbol for both these sources, Finance::Quote::SymbolTranslation 
can be used to translate common symbol name to one specific to each source.

For example, the symbol used for Infosys is INFY and INFTEC for NseIndia and 
IciciDirect respectively.

To be able to use symbol INFY with india_source method with NseIndia and 
IciciDirect as sources, create / modify fq_symbol_translation.txt and add 
following line - 

  icici_direct INFY INFTEC

F::Q will ask both NseIndia and IciciDirect to fetch information about INFY, but 
IciciDirect will translate INFY to INFTEC, fetch information about INFTEC and  
return data as if it fetched data for INFY.

Similarly, to be able to use symbol INFTEC with india_source method with NseIndia and 
IciciDirect as sources, create / modify fq_symbol_translation.txt and add 
following line - 

  nse_india INFTEC INFY

fq_symbol_translation.txt file should be either in the current directory or in 
the home directory (on windows machines, in the directory given by USERPROFILE 
environment varibale)

=head1 LABELS RETURNED

this source returns following lables on success:

  currency - INR
  date - date in US (mm/dd/yyyy) format
  exchange - NSE / BSE
  isodate - date in ISO (yyyy-mm-dd) format
  last - closing price in INR
  method - icici_direct
  symbol - ICICI Direct code
  success - 0 = failure / 1 = success
  errormsg - only if success is 0
  
=head1 SEE ALSO

ICICI Direct web site http://www.icicidirect.com.

Finance::Quote,
Finance::Quote::SymbolTranslation

=cut
