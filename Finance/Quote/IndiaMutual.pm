# 2020-04-08 - http://www.amfiindia.com/spages/NAVAll.txt
# 2013-11-24 - http://portal.amfiindia.com/spages/NAV0.txt
# Version 0.1 preliminary version using Cdnfundlibrary.pm v0.4 as an example

package Finance::Quote::IndiaMutual;
require 5.004;

use strict;
use warnings;
use vars qw($VERSION $AMFI_URL $AMFI_NAV_LIST $AMFI_MAIN_URL);
use Finance::Quote::SymbolTranslation qw(symbol_translation);

#use LWP::UserAgent;
#use HTTP::Request::Common;
#use HTTP::Status;
#use HTML::TableExtract;

$VERSION = '1.18';

# URLs of where to obtain information.

$AMFI_MAIN_URL = "http://www.amfiindia.com";
$AMFI_URL = $AMFI_MAIN_URL . "/spages/NAVAll.txt";
$AMFI_NAV_LIST = $ENV{TEMP} ? "$ENV{TEMP}/amfinavlist.txt" : "/tmp/amfinavlist.txt";
my $debug = 0;

sub methods {
  return (
    indiamutual => \&amfiindia,
    amfiindia   => \&amfiindia
  );
}

{
  my @labels = qw/method source link name currency date isodate nav rprice sprice/;
  sub labels {
    return (
      indiamutual => \@labels,
      amfiindia   => \@labels
    );
  }
}

#
# =======================================================================

sub amfiindia {
  my $quoter = shift;
  my @symbols = @_;

  $debug = 0;
  
  if (@symbols && $symbols[0] =~ m/^Debug$|^Debug=(\d+)$/) { # if first symbol is Debug, enable debugging
    if (defined $1) {
      $debug = $1;
    }
    else {
      $debug = 1;
    }
    shift @symbols;
  }
  
  return unless @symbols;

  # Local Variables
  my(%fundquote, %allquotes);
  my($ua, $url, $reply);

  $ua = $quoter->user_agent;

	my ($ref_trans_1, $ref_trans_2) = symbol_translation('amfiindia', \@symbols, 1);
	
  $url = $AMFI_URL;
  if ($debug & 1) {
    print "URL = $url\n";
  }

  $reply = $ua->mirror($url, $AMFI_NAV_LIST);

  # Make sure something is returned
  unless ($reply->is_success or $reply->code == 304) {
  # unless ($reply->is_success) {
    foreach my $symbol (@symbols) {
      $fundquote{$symbol,"success"} = 0;
      $fundquote{$symbol,"errormsg"} = "HTTP failure : " . $reply->status_line . " : " . $reply->code;
    }
    return wantarray ? %fundquote : \%fundquote;
  }

  if ($debug & 2) {
    print "AMFI_NAV_LIST = $AMFI_NAV_LIST\n";
  }
  
  open NAV, $AMFI_NAV_LIST or die "Unexpected error in opening file: $!\n";

  # Scheme Code;Scheme Data
  while (<NAV>) {
    next if !/\;/;
    chomp;
    s/\r//;
    my ($symbol, @data) = split /\s*\;\s*/;
    next unless exists $ref_trans_2->{uc($symbol)};
    $allquotes{$symbol} = \@data;
  }
  close(NAV);

  foreach my $symbol (@symbols) {
    $fundquote{$symbol, "symbol"} = $ref_trans_1->{uc($symbol)};
    $fundquote{$symbol, "currency"} = "INR";
    $fundquote{$symbol, "source"} = $AMFI_MAIN_URL;
    $fundquote{$symbol, "link"} = $url;
    $fundquote{$symbol, "method"} = "amfiindia";

    my $data = $allquotes{$ref_trans_1->{uc($symbol)}};
# Old format - 
#		Scheme Code
#		Scheme Name
#		Net Asset Value
#		Repurchase Price
#		Sale Price
#		Date
# New format - 2013-11-24
# 	Scheme Code
#		ISIN Div Payout / ISIN Growth   [0]
#		ISIN Div Reinvestment           [1]
#		Scheme Name                     [2]
#		Net Asset Value                 [3]
#		Repurchase Price                [4]
#		Sale Price                      [5]
#		Date                            [6]
# New format - 2020-04-08
# 	Scheme Code
#		ISIN Div Payout / ISIN Growth   [0]
#		ISIN Div Reinvestment           [1]
#		Scheme Name                     [2]
#		Net Asset Value                 [3]
#		Date                            [4]

    if ($data) {
	    $fundquote{$symbol, "name"} = $data->[2];
	    $fundquote{$symbol, "nav"} = $data->[3];
	    $fundquote{$symbol, "rprice"} = 0;
	    $fundquote{$symbol, "sprice"} = 0;
	    $quoter->store_date(\%fundquote, $symbol, {eurodate => $data->[4]});
	    $fundquote{$symbol, "success"} = 1;
    } else {
	    $fundquote{$symbol, "success"} = 0;
	    $fundquote{$symbol, "errormsg"} = "Fund not found";
    }
  } 

  return wantarray ? %fundquote : \%fundquote;
}

1;

=head1 NAME

Finance::Quote::IndiaMutual  - Obtain Indian mutual fund prices from amfiindia.com

=head1 SYNOPSIS

    use Finance::Quote;

    $q = Finance::Quote->new;

    %stockinfo = $q->fetch("indiamutual", "amfiindia-code"); # Can
failover to other methods
    %stockinfo = $q->fetch("amfiindia", "amfiindia-code"); # Use this
module only.

    # NOTE: currently no failover methods exist for indiamutual

=head1 DESCRIPTION

This module obtains information about Indian Mutual Fund prices from the 
Association of Mutual Funds India website amfiindia.com.
The information source "indiamutual" can be used
if the source of prices is irrelevant, and "amfiindia" if you
specifically want to use information downloaded from amfiindia.com.

=head1 AMFIINDIA-CODE

In India a mutual fund does not have a unique global symbol identifier.

This module uses an id that represents the mutual fund on an id used by
amfiindia.com.  You can the fund is from the AMFI web site 
http://amfiindia.com/downloadnavopen.asp.

=head1 LABELS RETURNED

Information available from amfiindia may include the following labels:

method link source name currency nav rprice sprice.  The link
label will be a url location for the NAV list table for all funds.

=head1 NOTES

amfiindia.com provides a link to download a text file containing all the 
NAVs. This file is mirrored in a local file /tmp/amfinavlist.txt. The local 
mirror serves only as a cache and can be safely removed. 

Currently NAVs of Open-Ended funds are supported. 

=head1 SEE ALSO

AMFI india website - http://amfiindia.com/

Finance::Quote

=cut

