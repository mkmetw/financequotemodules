#
#    Copyright (C) 1998, Dj Padzensky <djpadz@padz.net>
#    Copyright (C) 1998, 1999 Linas Vepstas <linas@linas.org>
#    Copyright (C) 2000, Yannick LE NY <y-le-ny@ifrance.com>
#    Copyright (C) 2000, Paul Fenwick <pjf@cpan.org>
#    Copyright (C) 2000, Brent Neal <brentn@users.sourceforge.net>
#    Copyright (C) 2013, Manoj Kumar
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
#    02111-1307, USA
#
# This code derived from Padzensky's work on package Finance::YahooQuote,
# but extends its capabilites to encompas a greater number of data sources.
#
# This code is derived from Finance::Quote::IndiaMutual 1.18
#
# This code was developed as part of GnuCash <http://www.gnucash.org/>

package Finance::Quote::NseIndia;
require 5.005;

use strict;
use warnings;
use diagnostics;
use vars qw($VERSION);
use Finance::Quote::SymbolTranslation qw(symbol_translation);
use IO::Uncompress::Unzip qw(unzip $UnzipError);
use Data::Dumper;

$VERSION = '1.19';

# https://archives.nseindia.com/content/historical/EQUITIES/2020/APR/cm08APR2020bhav.csv.zip
my $URL_NSE = 'http://archives.nseindia.com';
my $URL = $URL_NSE;
my $BHAV_ZIP = $ENV{TEMP} ? "$ENV{TEMP}/nse_bhav.zip" : '/tmp/nse_bhav.zip';
my $BHAV_CSV = $ENV{TEMP} ? "$ENV{TEMP}/nse_bhav.csv" : '/tmp/nse_bhav.csv';


sub methods {
	return (
		india_shares => \&nse_india_bhav,
		nse_india => \&nse_india_bhav,
		nse_india_bhav => \&nse_india_bhav
	);
}


{
	my @labels = qw/currency date isodate method price success symbol isin series/;
	
	sub labels {
		return (
			india_shares => \@labels,
			nse_india => \@labels,
			nse_india_bhav => \@labels
		);
	}
}


sub get_bhav_url {
	my $time = shift;
  my $with_0 = shift;
	my @months = ('JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC');
	my ($mday, $month, $year);
	
	(undef, undef, undef, $mday, $month, $year, undef, undef, undef) = localtime($time);
	$month = $months[$month];
	$year += 1900;
  $mday = ($with_0 && length($mday) < 2) ? ('0' . $mday) : $mday;
  
  # https://archives.nseindia.com/content/historical/EQUITIES/2020/APR/cm08APR2020bhav.csv.zip
  return("$URL/content/historical/EQUITIES/$year/$month/cm$mday$month${year}bhav.csv.zip");
}


sub url {
  if (! @_) {
    return($URL);
  }
  
  if ($_[0] eq '') {
    $URL = $URL_NSE;
  }
  else {
    $URL = shift;
  }
}


sub nse_india_bhav {
	my $quoter = shift;
	my @symbols = @_;
  my $debug = 0;
  
  if (@symbols && $symbols[0] =~ m/^Debug$|^Debug=(\d+)$/) { # if first symbol is Debug, enable debugging
    if (defined $1) {
      $debug = $1;
    }
    else {
      $debug = 1;
    }
    shift @symbols;
  }

	return unless @symbols;

	my (%info, $errormsg, $fh, $ref_trans_2);
	my ($ua, $url, $reply);
	
	$ua = $quoter->user_agent->clone;
	$ua->default_header('Accept' => '*/*', 'User-Agent' => 'FQ/1.18');
  
	for (my ($nb_days, $time, $last_url) = (0, time, ''); $nb_days < 14; $nb_days++, $time -= 24 * 60 * 60) {
    for (my $with_0 = 1; $with_0 >= 0; $with_0--) {
      $url = get_bhav_url($time, $with_0);
      if ($debug & 1) {
        print "URL = $url\n";
      }
      
      if ($url eq $last_url) {
        if ($debug & 1) {
          print "URL same as the last one\n";
        }
        next;
      }
      
      $last_url = $url;
      $reply = $ua->mirror($url, $BHAV_ZIP);

      if ($debug & 2) {
        print "==\n", Dumper($reply), "==\n";
      }
      
      if ($debug & 4) {
        print "==\n", Dumper($reply->request), "==\n";
      }
      
      if ($debug & 8) {
        print "==\n", Dumper($reply->request->uri), "==\n";
      }
      
      if ($debug & 16) {
        print "==\n", Dumper($reply->request->uri->as_string), "==\n";
      }
      
      if ($debug & 32) {
        print "Reply = ", $reply->is_success ? "Successful" : "Unsuccessful", " : ", $reply->status_line, "\n";
      }
      
      my $uri = $reply->request->uri->as_string;
      if ($reply->is_success && ($uri =~ /nseindia.com\/404/)) {
        if ($debug & 64) {
          print "Deleting $BHAV_ZIP\n";
        }
        
        if (! unlink $BHAV_ZIP) {
          if ($debug & 64) {
            print "Could not delete $BHAV_ZIP: $!\n";
          }
        }
      }
      
      last if (($reply->is_success || $reply->code == 304) && ($uri !~ /nseindia.com\/404/));
    }
    
    my $uri = $reply->request->uri->as_string;
    last if (($reply->is_success || $reply->code == 304) && ($uri !~ /nseindia.com\/404/));
	}
	
  my $uri = $reply->request->uri->as_string;
	unless (($reply->is_success || $reply->code == 304) && ($uri !~ /nseindia.com\/404/)) {
		$errormsg = "HTTP failure : " . $reply->status_line;
	}
	
	unless ($errormsg) {
		if (! unzip $BHAV_ZIP => $BHAV_CSV) {
			$errormsg = "Unzip failure : $UnzipError";
		}
	}
	
	unless ($errormsg) {
		if (! open $fh, '<', $BHAV_CSV) {
			$errormsg = "Open failure : $!";
		}
	}
	
	unless ($errormsg) {
		(undef, $ref_trans_2) = symbol_translation('nse_india', \@symbols, 1);
		
		# SYMBOL,SERIES,OPEN,HIGH,LOW,CLOSE,LAST,PREVCLOSE,TOTTRDQTY,TOTTRDVAL,TIMESTAMP,TOTALTRADES,ISIN,
		while (<$fh>) {
			my @data = split /\s*,s*/;
			next unless (@data >= 13);
      my ($symbol, $symbol_nse, $price, $date, $isin, $series);
      ($symbol_nse, $series, $price, $date, $isin) = ($data[0], $data[1], $data[5], $data[10], $data[12]); # use ISIN for lookup
      $symbol = uc($isin);
			next unless exists $ref_trans_2->{$symbol};
			$symbol = $ref_trans_2->{$symbol};
			
			$info{$symbol, 'symbol'} = $symbol_nse;
			$info{$symbol, 'currency'} = 'INR';
			$info{$symbol, 'price'} = $price;
			$quoter->store_date(\%info, $symbol, {eurodate => $date});
			$info{$symbol, 'isin'} = $isin;
			$info{$symbol, 'series'} = $series;
      $info{$symbol, 'method'} = 'nse_india_bhav';
			$info{$symbol, 'success'} = 1;
		}
		
		close($fh);
	}
	
	unless ($errormsg) {
		$errormsg = 'Stock lookup failed';
	}
	
	foreach my $symbol (@symbols) {
		next if (exists $info{$symbol, 'success'});
		$info{$symbol, 'method'} = 'nse_india_bhav';
		$info{$symbol, 'errormsg'} = $errormsg;
		$info{$symbol, 'success'} = 0;
	}
	
	return wantarray ? %info : \%info;
}


1;


=head1 NAME

Finance::Quote::NseIndia  - Fetch quotes from NSE, India http://www.nseindia.com

=head1 SYNOPSIS

  use Finance::Quote;

  $q = Finance::Quote->new();

  %info = $q->fetch('india_shares', 'INE144J01027'); # Failover to other methods ok. INE144J01027 is ISIN for 20MICRONS.
  %info = $q->fetch('nse_india', 'INE144J01027'); # Use this module only. INE144J01027 is ISIN for 20MICRONS.

=head1 DESCRIPTION

This module obtains information about securities listed on NSE, India from NSE, India web site http://www.nseindia.com.
This module uses ISIN for lookup.

The information is taken from the Bahv copy file (zipped CSV) prepared by NSE at the end of the trading day.
This file is mirrored in a local file.

This module uses Finance::Quote::SymbolTranslation for symbol translation. Source name for this purpose is nse_india.

=head1 SYMBOL TRANSLATION

If failover method india_shares is used with NseIndia and IciciDirect as 
sources, the same symbol can not be used with both these providers.

In order to use the same symbol for both these sources, Finance::Quote::SymbolTranslation 
can be used to translate common symbol name to the one specific to each source.

For example, the symbol used for Infosys are INE009A01021 and INFTEC for NseIndia and 
IciciDirect respectively.

To be able to use symbol INE009A01021 with india_shares method with NseIndia and 
IciciDirect as sources, create / modify fq_symbol_translation.txt and add 
following line - 

  icici_direct INE009A01021 INFTEC

F::Q will ask both NseIndia and IciciDirect to fetch information about INE009A01021, but 
IciciDirect will translate INE009A01021 to INFTEC, fetch information about INFTEC and  
return data as if it fetched data for INE009A01021.

Similarly, to be able to use symbol INFTEC with india_shares method with NseIndia and 
IciciDirect as sources, create / modify fq_symbol_translation.txt and add 
following line - 

  nse_india INFTEC INE009A01021

fq_symbol_translation.txt file should be either in the current directory or in 
the home directory (on windows machines, in the directory given by USERPROFILE 
environment varibale)

=head1 LABELS RETURNED

this source returns following lables on success:

  currency - INR
  date     - date in US (mm/dd/yyyy) format
  isodate  - date in ISO (yyyy-mm-dd) format
  method   - nse_india_bhav
  price    - closing price in INR
  symbol   - NSE symbol
  isin     - ISIN
  series   - NSE series
  success  - 0 = failure / 1 = success
  errormsg - only if success is 0

=head1 SEE ALSO

NSE, India web site http://www.nseindia.com.

Finance::Quote,
Finance::Quote::SymbolTranslation

=cut
