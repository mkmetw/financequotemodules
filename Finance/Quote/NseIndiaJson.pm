#
#    Copyright (C) 1998, Dj Padzensky <djpadz@padz.net>
#    Copyright (C) 1998, 1999 Linas Vepstas <linas@linas.org>
#    Copyright (C) 2000, Yannick LE NY <y-le-ny@ifrance.com>
#    Copyright (C) 2000, Paul Fenwick <pjf@cpan.org>
#    Copyright (C) 2000, Brent Neal <brentn@users.sourceforge.net>
#    Copyright (C) 2013, Manoj Kumar
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
#    02111-1307, USA
#
# This code derived from Padzensky's work on package Finance::YahooQuote,
# but extends its capabilites to encompas a greater number of data sources.
#
# This code was developed as part of GnuCash <http://www.gnucash.org/>

package Finance::Quote::NseIndiaJson;
require 5.005;

use strict;
use warnings;
use JSON;
use URI::Escape;
use Finance::Quote::SymbolTranslation qw(symbol_translation);
use Data::Dumper qw(Dumper);
use HTML::TreeBuilder;
use vars qw($VERSION);

#our ($VERSION, $MAIN_URL, $URL);

$VERSION = '1.18';

#$MAIN_URL = 'http://www.nseindia.com';
#$URL = $MAIN_URL . '/live_market/dynaContent/live_watch/get_quote/ajaxGetQuoteJSON.jsp?symbol=%s&series=%s';
#$URL = $MAIN_URL . '/live_market/dynaContent/live_watch/get_quote/GetQuote.jsp?symbol=%s&series=%s';
my $URL_ORIG = 'http://www.nseindia.com/live_market/dynaContent/live_watch/get_quote/GetQuote.jsp?symbol=%s&series=%s';
my $URL = $URL_ORIG;
my $debug = 0;

# https://www.nse-india.com/live_market/dynaContent/live_watch/get_quote/GetQuote.jsp?symbol=INFY&illiquid=0&smeFlag=0&itpFlag=0
# https://www.nse-india.com/live_market/dynaContent/live_watch/get_quote/GetQuote.jsp?symbol=NHAI&series=N1

sub methods {
  return (
    nse_india_json => \&nse_india_json,
    nse_india => \&nse_india_json,
    india_shares => \&nse_india_json
  );
}

{
  my @labels = ('currency', 'date', 'isodate', 'method', 'price', 'symbol', 'isin', 'series', 'name');
  sub labels {
    return (
      nse_india_json => \@labels,
      nse_india => \@labels,
      india_shares => \@labels
    );
  }
}

sub nse_india_json {
  my $quoter = shift;
  my @symbols = @_;
  
  if (@symbols && $symbols[0] =~ m/^Debug$|^Debug=(\d+)$/) { # if first symbol is Debug, enable debugging
    if (defined $1) {
      $debug = $1;
    }
    else {
      $debug = 1;
    }
    shift @symbols;
  }

  return unless @symbols;

  my (%info, %series);
  my ($symbol);
  my ($ref_trans_1, $ref_trans_2);
  
  ($ref_trans_1, $ref_trans_2) = symbol_translation('nse_india_json', \@symbols, 1);
  foreach (keys %{$ref_trans_2}) {
    my ($p1, $p2) = split /\./;
    if (! $p2) {
      $p2 = '';
    }
    push @{$series{uc($p2)}}, uc($p1);
  }
  
  foreach my $p2 (keys %series) {
    get_nse_data($quoter, \%info, $series{$p2}, $p2, $ref_trans_2, $debug);
  }
  
  foreach $symbol (@symbols) {
    next if (exists $info{$symbol, 'success'});
    my ($p1, $p2) = split /\./, $ref_trans_1->{uc($symbol)};
    if (! $p2) {
      $p2 = '';
    }
    
    my $errormsg = get_nse_data($quoter, \%info, [$p1], $p2, $ref_trans_2, $debug);
    if ($errormsg) {
      $info{$symbol, 'method'} = 'nse_india_json';
      $info{$symbol, 'errormsg'} = $errormsg;
      $info{$symbol, 'success'} = 0;
    }
  }
  
  foreach $symbol (@symbols) {
    next if (exists $info{$symbol, 'success'});
    $info{$symbol, 'method'} = 'nse_india_json';
    $info{$symbol, 'errormsg'} = 'Stock lookup failed';
    $info{$symbol, 'success'} = 0;
  }
  
  return wantarray ? %info : \%info;
}


sub get_nse_data {
  my $quoter = shift;
  my $ref_info = shift;
  my $ref_symb = shift;
  my $series = shift;
  my $ref_trans_2 = shift;
  my $debug = shift;
  my ($url, $response, $json);
  my ($day, $month, $year, $price, $symbol);

  $url = sprintf($URL, uri_escape(join(',', @{$ref_symb})), $series || 'EQ');
  if ($debug & 1) {
    print "URL = $url\n";
  }
  $response = $quoter->user_agent->get($url, 'Accept' => '*/*', 'User-Agent' => 'FQ/1.18');

  if ($response->is_success) {
    if ($debug & 16) {
      print "Content:\n";
      print "=====\n";
      print $response->decoded_content;
      print "\n=====\n\n";
    }
    
    my $root = HTML::TreeBuilder->new();
    
    $root->utf8_mode(1); # avoid warning Parsing of undecoded UTF-8 will give garbage when decoding entities at
    
    $root->parse_content($response->content);
    if ($debug & 32) {
      print "Tree:\n";
      print "=====\n";
      $root->dump();
      print "\n=====\n\n";
    }
    
    $json = extract_json($debug, $root);
    
    $json  = decode_json $json;
    dump_json($debug, $json);
    
    if (exists $json->{data}) {
      foreach my $ref (@{$json->{data}}) {
        if (! $ref->{symbol}) {
          if ($debug & 8) {
            print "nse_india_json: symbol not found in data\n";
          }
          next;
        }
        
        if (! $ref->{series}) {
          if ($debug & 8) {
            print "nse_india_json: series not found in data\n";
          }
          next;
        }
        
        $ref->{symbol} =~ s/&amp;/&/;
        
        if (! $series && $ref_trans_2->{uc($ref->{symbol})}) {
          $symbol = $ref_trans_2->{uc($ref->{symbol})};
        }
        elsif ($series && uc($ref->{series}) eq $series && $ref_trans_2->{uc($ref->{symbol} . '.' . $series)}) {
          $symbol = $ref_trans_2->{uc($ref->{symbol} . '.' . $series)};
        }
        else {
          if ($debug & 8) {
            print "nse_india_json: symbol received does not match symbol requested\n";
          }
          next;
        }
        
        if ($json->{tradedDate} && $json->{tradedDate} =~ m/(\d+)(\D+)(\d+)/) {
          ($day, $month, $year) = ($1, $2, $3);
        }
        elsif ($ref->{secDate} && $ref->{secDate} =~ m/(\d+)(\D+)(\d+)/) {
          ($day, $month, $year) = ($1, $2, $3);
        }
        else {
          if ($debug & 8) {
            print "nse_india_json: secDate, tradedDate not found in data\n";
          }
          next;
        }

        $price = $ref->{closePrice};
        if (! ($price && $price =~ m/\d/)) {
          $price = $ref->{lastPrice};
          if (! ($price && $price =~ m/\d/)) {
            if ($debug & 8) {
              print "nse_india_json: closePrice, lastPrice not found in data\n";
            }
            next;
          }
        }
        $price =~ s/,//g;
        
        $quoter->store_date($ref_info, $symbol, {day => $day, month => $month, year => $year});
        $ref_info->{$symbol, 'price'} = $price;
        $ref_info->{$symbol, 'symbol'} = $ref->{symbol};
        $ref_info->{$symbol, 'currency'} = 'INR';
        $ref_info->{$symbol, 'name'} = $ref->{companyName};
        $ref_info->{$symbol, 'series'} = $ref->{series};
        $ref_info->{$symbol, 'isin'} = $ref->{isinCode};
        $ref_info->{$symbol, 'method'} = 'nse_india_json';
        $ref_info->{$symbol, 'success'} = 1;
      }
    }
    else {
      return('Stock lookup failed');
    }
  }
  else {
    return("HTTP failure : " . $response->status_line);
  }
  
  return('');
}


sub dump_json {
  my $debug = shift;
  my $json = shift;
  
  if ($debug & 2) {
    my $old_value = $Data::Dumper::Sortkeys;
    $Data::Dumper::Sortkeys = 1;
    print Dumper($json);
    $Data::Dumper::Sortkeys = $old_value;
  }
  elsif ($debug & 4) {
    print Dumper($json);
  }
}


sub extract_json {
  my $debug = shift;
  my $root = shift;
  my ($element);
  
  $element = $root->look_down('_tag', 'div', 'id' => 'responseDiv');
  if (! $element) {
    if ($debug & 64) {
      print "extract_json: div responseDiv not found\n";
    }
    return '';
  }
  
  if ($debug & 64) {
    print "extract_json: JSON = ", $element->as_text(), "\n";
  }
  
  return $element->as_text();
}


sub url {
  if (! @_) {
    return($URL);
  }
  
  if ($_[0] eq '') {
    $URL = $URL_ORIG;
  }
  else {
    $URL = shift;
  }
}


1;

=head1 NAME

Finance::Quote::NseIndiaJson  - Fetch quotes from NSE, India http://www.nseindia.com

=head1 SYNOPSIS

    use Finance::Quote;

    $q = Finance::Quote->new();

    %info = $q->fetch('india_shares', 'TCS'); # Failover to other methods ok.
    %info = $q->fetch('nse_india', 'TCS'); # Failover to other methods ok.
    %info = $q->fetch('nse_india_json', 'TCS'); # Use this module only to get TCS price in default series.
    %info = $q->fetch('nse_india_json', 'INFY.EQ'); # Use this module only to get INFY price in EQ series.
    %info = $q->fetch('nse_india_json', 'NHAI.N1'); # Use this module only to get NHAI price in N1 series.

=head1 DESCRIPTION

This module obtains information about india shares from NSE, India web site http://www.nseindia.com.

This module uses Finance::Quote::SymbolTranslation for symbol translation. Source name for this purpose is nse_india_json.

=head1 LABELS RETURNED

this source returns following lables on success:

  currency - INR
  date - date in US (mm/dd/yyyy) format
  isodate - date in ISO (yyyy-mm-dd) format
  method - nse_india_json
  price - closing / last price in INR
  name - company name
  symbol - NSE symbol
  isin - ISIN
  series - NSE series
  success - 0 = failure / 1 = success
  errormsg - only if success is 0
  
=head1 SEE ALSO

NSE, India web site http://www.nseindia.com.

Finance::Quote,
Finance::Quote::SymbolTranslation

=cut
