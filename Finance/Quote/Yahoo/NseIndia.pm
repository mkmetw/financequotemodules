#    Copyright (C) 1998, Dj Padzensky <djpadz@padz.net>
#    Copyright (C) 1998, 1999 Linas Vepstas <linas@linas.org>
#    Copyright (C) 2000, Yannick LE NY <y-le-ny@ifrance.com>
#    Copyright (C) 2000, Paul Fenwick <pjf@cpan.org>
#    Copyright (C) 2000, Brent Neal <brentn@users.sourceforge.net>
#    Copyright (C) 2013, Manoj Kumar
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
#    02111-1307, USA
#
# This code derived from Padzensky's work on package Finance::YahooQuote,
# but extends its capabilites to encompas a greater number of data sources.
#
# This code was developed as part of GnuCash <http://www.gnucash.org/>

package Finance::Quote::Yahoo::NseIndia;
require 5.005;

use strict;
use warnings;
use diagnostics;
use Finance::Quote::Yahoo::BaseJson qw/yahoo_request base_yahoo_labels/;
use Finance::Quote::SymbolTranslation qw(symbol_translation);

use vars qw($VERSION $URL);

$VERSION = '1.18';

$URL = 'http://finance.yahoo.co.in/webservice/v1/symbols/%s/quote?format=json';

sub methods {
  return (
    'india_shares' => \&yahoo_nse_india, 
    'nse_india' => \&yahoo_nse_india, 
    'yahoo_nse_india' => \&yahoo_nse_india
  );
}


{
  my @labels = (base_yahoo_labels(), 'method');
  
  sub labels {
    return (
      'india_shares' => \@labels, 
      'nse_india' => \@labels, 
      'yahoo_nse_india' => \@labels
    );
  }
}


sub yahoo_nse_india {
	my $quoter = shift;
	my @symbols = @_;
  my $debug = 0;
  
	if (@symbols && $symbols[0] eq 'Debug') { # if first symbol is Debug, enable debugging
    $debug = 1;
    shift @symbols;
  }

	return unless @symbols;	# Nothing if no symbols.
  
	my ($ref_trans_1, $ref_trans_2) = symbol_translation('yahoo_nse_india', \@symbols, 1);
	my %info = yahoo_request($quoter, $URL, [keys %$ref_trans_2], '.NS', undef, $debug);
  
	foreach my $keyname (sort keys %info) {
		my ($stock, $key) = split('\034', $keyname);
		my $old = $ref_trans_2->{uc($stock)};
		next if ($old eq $stock);
		$info{$old, $key} = $info{$keyname}; # create entry for the original symbol
		delete $info{$keyname}; # delete entry for the translated symbol
  }
  
	foreach my $symbol (@symbols) {
		unless (exists $info{$symbol, 'success'}) {
			$info{$symbol, 'success'} = 0;
			$info{$symbol, 'errormsg'} = 'Stock lookup failed';
		}
    
		$info{$symbol, 'method'} = 'yahoo_nse_india';
	}
  
	return wantarray ? %info : \%info;
}


1;

=head1 NAME

Finance::Quote::Yahoo::NseIndia - Fetch quotes from Yahoo

=head1 SYNOPSIS

    use Finance::Quote;
    $q = Finance::Quote->new;

    %info = $q->fetch('india_shares', 'TCS'); # Failover to other methods ok.
    %info = $q->fetch('nse_india', 'TCS'); 		# Failover to other methods ok.
    %info = $q->fetch('yahoo_nse_india', 'TCS'); 		# Use this module only.

=head1 DESCRIPTION

This module fetches information from Yahoo.  Symbols should be
provided in without the exchange suffix (.NS).

This module provides "india_shares", "nse_india" and "yahoo_nse_india" methods.
The "india_shares" and "nse_india" methods should be used if failover methods are desirable.
The "yahoo_nse_india" method should be used you desire to only fetch
information from Yahoo.

This module can be loaded explicitly by specifying the parameter "Yahoo::NseIndia" to
Finance::Quote->new() or putting it in FQ_LOAD_QUOTELET environment variable.

Information obtained by this module may be covered by Yahoo's terms
and conditions.  See http://finance.yahoo.com/ for more details.

This module uses Finance::Quote::SymbolTranslation for symbol translation. Source name for this purpose is yahoo_nse_india.

=head1 LABELS RETURNED

This module returns all the standard labels (where available) provided
by Yahoo.  See Finance::Quote::Yahoo::BaseJson for a list of these.  The
currency label is also returned.

=head1 SEE ALSO

Yahoo Finance, http://finance.yahoo.com/

Finance::Quote::Yahoo::BaseJson
Finance::Quote
Finance::Quote::SymbolTranslation

=cut
