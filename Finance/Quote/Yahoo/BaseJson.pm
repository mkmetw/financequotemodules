#    Copyright (C) 1998, Dj Padzensky <djpadz@padz.net>
#    Copyright (C) 1998, 1999 Linas Vepstas <linas@linas.org>
#    Copyright (C) 2000, Yannick LE NY <y-le-ny@ifrance.com>
#    Copyright (C) 2000, Paul Fenwick <pjf@cpan.org>
#    Copyright (C) 2000, Brent Neal <brentn@users.sourceforge.net>
#    Copyright (C) 2013, Manoj Kumar
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
#    02111-1307, USA
#
# This code derived from Padzensky's work on package Finance::YahooQuote,
# but extends its capabilites to encompas a greater number of data sources.
#
# This package provides a base class for the various Yahoo services,
# and is based upon code by Xose Manoel Ramos <xmanoel@bigfoot.com>.
# Improvements based upon patches supplied by Peter Thatcher have
# also been integrated.

package Finance::Quote::Yahoo::BaseJson;
require 5.005;

use strict;
use warnings;
use diagnostics;
use Exporter;
use JSON;
use URI::Escape;
use Data::Dumper;

use vars qw/@FIELDS $VERSION $MAX_REQUEST_SIZE @ISA @EXPORT @EXPORT_OK/;

@ISA = qw/Exporter/;
@EXPORT = qw//;
@EXPORT_OK = qw/yahoo_request base_yahoo_labels/;

$VERSION = '1.18';

# This is the maximum number of stocks we'll batch into one operation.
# If this gets too big (>50 or thereabouts) things will break because
# some proxies and/or webservers cannot handle very large URLS.

$MAX_REQUEST_SIZE = 40;

@FIELDS = qw/currency_set_by_fq currency date isodate name price symbol volume/;

# This returns a list of labels that are provided, so that code
# that make use of this module can know what it's dealing with.
# It also means that if we extend the code in the future to provide
# more information, we simply need to change this in one spot.

sub base_yahoo_labels {
	return (@FIELDS);
}

# Yahoo uses a suffix on the stock symbol to denote the exchange on
# which the stock is traded.  Use this suffix to map from the stock
# symbol to the currency in which its prices are reported.

my %currency_tags = (
                # Country		City/Exchange Name
  US  => "USD", # USA		    AMEX, Nasdaq, NYSE
  A   => "USD", # USA		    American Stock Exchange (ASE)
  B   => "USD", # USA		    Boston Stock Exchange (BOS)
  N   => "USD", # USA		    Nasdaq Stock Exchange (NAS)
  O   => "USD", # USA		    NYSE Stock Exchange (NYS)
  OB  => "USD", # USA		    OTC Bulletin Board
  PK  => "USD", # USA		    Pink Sheets
  X   => "USD", # USA		    US Options
  BA  => "ARS", # Argentina	Buenos Aires
  VI  => "EUR", # Austria		Vienna
  AX  => "AUD", # Australia
  SA  => "BRL", # Brazil		Sao Paolo
  BR  => "EUR", # Belgium		Brussels
  TO  => "CAD", # Canada		Toronto
  V   => "CAD", # Toronto   Venture
  SN  => "CLP", # Chile		  Santiago
  SS  => "CNY", # China		  Shanghai
  SZ  => "CNY", # China     Shenzhen
  CO  => "DKK", # Denmark		Copenhagen
  PA  => "EUR", # France		Paris
  BE  => "EUR", # Germany		Berlin
  BM  => "EUR", # 		      Bremen
  D   => "EUR", # 		      Dusseldorf
  F   => "EUR", # 		      Frankfurt
  H   => "EUR", # 		      Hamburg
  HA  => "EUR", # 		      Hanover
  MU  => "EUR", # 		      Munich
  SG  => "EUR", # 		      Stuttgart
  DE  => "EUR", # 		      XETRA
  HK  => "HKD", # Hong Kong
  BO  => "INR", # India		  Bombay
  CL  => "INR", # 		      Calcutta
  NS  => "INR", # 		      National Stock Exchange
  JK  => "IDR", # Indonesia	Jakarta
  I   => "EUR", # Ireland		Dublin
  TA  => "ILS", # Israel		Tel Aviv
  MI  => "EUR", # Italy		  Milan
  KS  => "KRW", # Korea	    Korea Stock Exchange
  KQ  => "KRW", # 		      KOSDAQ
  KL  => "MYR", # Malaysia	Kuala Lampur
  MX  => "MXP", # Mexico
  NZ  => "NZD", # New Zealand
  AS  => "EUR", # Netherlands	Amsterdam
  OL  => "NOK", # Norway    Oslo
  LM  => "PEN", # Peru		  Lima
  IN  => "EUR", # Portugal	Lisbon
  SI  => "SGD", # Singapore
  BC  => "EUR", # Spain		  Barcelona
  BI  => "EUR", # 		      Bilbao
  MF  => "EUR", # 		      Madrid Fixed Income
  MC  => "EUR", # 		      Madrid SE CATS
  MA  => "EUR", # 		      Madrid
  VA  => "EUR", # 		      Valence
  ST  => "SEK", # Sweden		Stockholm
  S   => "CHF", # Switzerland	Zurich
  TW  => "TWD", # Taiwan		Taiwan Stock Exchange
  TWO => "TWD", # 		      OTC
  BK  => "THB", # Thialand	Thailand Stock Exchange
  TH  => "THB", # 		??? From Asia.pm, (in Thai Baht)
  L   => "GBP", # United Kingdom	London
);

# yahoo_request (restricted function)
#
# This function expects a Finance::Quote object, a base URL to use,
# a refernece to a list of symbols to lookup.  If a fourth argument is
# used then it will act as a suffix that needs to be appended to the stocks
# in order to obtain the correct information.  This function relies upon
# the fact that the various Yahoo's all work the same way.

sub yahoo_request {
	my $quoter = shift;
	my $base_url = shift;
	my @orig_symbols = @{shift()};
	my $suffix = shift || ''; # The suffix is used to specify particular markets.
	my $seperator = shift || ',';
  my $debug = shift // 0;
  
	my %info;
  my %symbols;
	my $ua = $quoter->user_agent;
  
  foreach (@orig_symbols) { # yahoo changes symbol name to upper case
    $symbols{uc($_ . $suffix)} = $_;
  }
  
	while (my @symbols = splice(@orig_symbols, 0, $MAX_REQUEST_SIZE)) {
		my $url = sprintf($base_url, uri_escape(join("$suffix$seperator", @symbols)) . $suffix);
		
    if ($debug) {
      print "URL = $url\n";
    }
		
		my $response = $ua->get($url);
		last unless $response->is_success;
    my $data_ref = decode_json $response->content;   # utf8 encoded json text
    
    if ($debug) {
      print Data::Dumper::Dumper($data_ref);
    }
    
    next unless exists $data_ref->{'list'};
    $data_ref = $data_ref->{'list'};
    next unless exists $data_ref->{'resources'};
    $data_ref = $data_ref->{'resources'};
    foreach (@{$data_ref}) {
      next unless exists $_->{'resource'};
      $_ = $_->{'resource'};
      next unless exists $_->{'fields'};
      $_ = $_->{'fields'};
      next unless exists $_->{'symbol'};
      next unless exists $_->{'price'};
      next unless exists $_->{'utctime'};
      $_->{'symbol'} =~ s/&amp;/&/;
      next unless exists $symbols{uc($_->{'symbol'})};
      my $symbol = $symbols{uc($_->{'symbol'})};

      my ($exchange) = $_->{'symbol'} =~ m/\.([A-Z]+)/;
      if (defined $exchange) {
        $info{$symbol, 'currency'} = $currency_tags{$exchange};
      } elsif (substr($_->{'symbol'}, 0, 1) ne '^') {
        $info{$symbol, 'currency'} = 'USD';
      }
      
      $info{$symbol, 'symbol'} = $_->{'symbol'};
      $info{$symbol, 'price'} = $_->{'price'};
      $info{$symbol, 'volume'} = $_->{'volume'};
      if (exists $_->{'name'}) {
        $_->{'name'} =~ s/&amp;/&/;
      }
      $info{$symbol, 'name'} = $_->{'name'};
			$quoter->store_date(\%info, $symbol, {isodate => $_->{'utctime'}});
      delete $info{$symbol, 'a'};
      $info{$symbol, 'success'} = 1;
    }
	}
  
	foreach (keys %symbols) {
    my $symbol = $symbols{$_};
		unless (exists $info{$symbol, 'success'}) {
			$info{$symbol, 'success'} = 0;
			$info{$symbol, 'errormsg'} = 'Stock lookup failed';
		}
	}
  
	return wantarray ? %info : \%info;
}


1;

=head1 NAME

Finance::Quote::Yahoo::BaseJson - Common functions for fetching Yahoo info using JSON API.

=head1 SYNOPSIS

Base functions for use by the Finance::Quote::Yahoo::* modules.

=head1 DESCRIPTION

This module is not called directly, rather it provides a set of
base functions which other Yahoo-related modules can use.  If you're
thinking of writing a module to fetch specific information from
Yahoo, then you might wish to look through the source code for
this module.

=head1 LABELS RETURNED

Most Yahoo functions will return a standard set of labels.  These
include (where available): currency date isodate name price symbol volume.

=head1 SEE ALSO

Finance::Quote::Yahoo::NseIndia, Finance::Quote::Yahoo::BseIndia.

=cut
