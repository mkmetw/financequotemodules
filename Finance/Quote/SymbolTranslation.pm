#
#    Copyright (C) 1998, Dj Padzensky <djpadz@padz.net>
#    Copyright (C) 1998, 1999 Linas Vepstas <linas@linas.org>
#    Copyright (C) 2000, Yannick LE NY <y-le-ny@ifrance.com>
#    Copyright (C) 2000, Paul Fenwick <pjf@cpan.org>
#    Copyright (C) 2000, Brent Neal <brentn@users.sourceforge.net>
#    Copyright (C) 2013, Manoj Kumar
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
#    02111-1307, USA
#
# This code derived from Padzensky's work on package Finance::YahooQuote,
# but extends its capabilites to encompas a greater number of data sources.
#
# This code was developed as part of GnuCash <http://www.gnucash.org/>

package Finance::Quote::SymbolTranslation;
require 5.005;

use strict;
use warnings;
use parent qw( Exporter );
our (@EXPORT_OK, $VERSION);

@EXPORT_OK = qw( symbol_translation );

$VERSION = '1.18';

sub symbol_translation {
	my $method = shift;
	my $ref = shift;
	my $case = shift;
	my $file_name = "fq_symbol_translation.txt";
	my (%trans_1, %trans_2, $fh, $symbol);
	my ($old, $new);
	
	foreach (@{$ref}) {
		$new = $old = $case ? ($case == 1 ? uc : lc) : $_;
		$trans_1{$old} = $new;
		$trans_2{$new} = $_;
	}
	
	if (! -e $file_name) {
		if ($^O eq 'MSWin32') {
			if (exists $ENV{USERPROFILE}) {
				$file_name = $ENV{USERPROFILE} . "\\" . $file_name;
			}
		}
		else {
			if (exists $ENV{HOME}) {
				$file_name = $ENV{HOME} . "/" . $file_name;
			}
		}
	}
	
	open($fh, "<", $file_name) || return (\%trans_1, \%trans_2);
	
	while (<$fh>) {
		next unless (/^\s*$method\s+(\S+)\s+(\S+)/); # method old new
		$old = $case ? ($case == 1 ? uc($1) : lc($1)) : $1;
		$new = $case ? ($case == 1 ? uc($2) : lc($2)) : $2;
		if (exists $trans_1{$old} && $old ne $new) {
			$trans_1{$old} = $new; # forward translation - old symbol to new symbol
			$trans_2{$new} = $trans_2{$old}; # reverse translation - new symbol to old (original) symbol
			delete $trans_2{$old};
		}
	}
	
	close($fh);
	
	return (\%trans_1, \%trans_2);
}


1;

=head1 NAME

Finance::Quote::SymbolTranslation - Translate symbols between various sources so that one name can be used to fetch quotes from different sources even though 
they use different names for the same security.

=head1 SYNOPSIS

  use Finance::Quote::SymbolTranslation qw(symbol_translation);
  my ($ref_1, $ref_2) = symbol_translation('icici_direct', ['HCLTECH'], 1);

  $ref_1 is reference to a hash mapping symbol(s) to the translated symbol(s).
  $ref_2 is reference to a hash mapping translated symbol(s) to the symbol(s).

  for example, if HCLTECH is translated to HCLTEC - 
  $ref_1->{'HCLTECH'} = 'HCLTEC'
  $ref_2->{'HCLTEC'} = 'HCLTECH'

=head1 AVAILABLE METHODS

=head2 SYMBOL_TRANSLATION

  my ($ref_1, $ref_2) = symbol_translation('icici_direct', ['HCLTECH', 'infy']);

Translate HCLTECH and infy using fq_symbol_translation.txt.

  my ($ref_1, $ref_2) = symbol_translation('icici_direct', ['HCLTECH', 'infy'], 1);

Translate HCLTECH and INFY using fq_symbol_translation.txt.

  my ($ref_1, $ref_2) = symbol_translation('icici_direct', ['HCLTECH', 'infy'], 2);
	
Translate hcltech and infy using fq_symbol_translation.txt.

=head1 TRANSLATION TABLE

This module uses fq_symbol_translation.txt for translation. Format of this file is:

<source>		<symbol>		<translated symbol>

where 
  <source> is the F::Q source,
  <symbol> is the symbol used by F::Q,
  <translated symbol> is the symbol used by the <source> for security <symbol>.
	
this file should be either in the current directory or in the home directory (on windows machines, in the directory given by USERPROFILE environment varibale)
	
=head1 SEE ALSO

Finance::Quote::NseIndia

=cut
