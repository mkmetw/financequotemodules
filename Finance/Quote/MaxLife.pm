#
#    Copyright (C) 1998, Dj Padzensky <djpadz@padz.net>
#    Copyright (C) 1998, 1999 Linas Vepstas <linas@linas.org>
#    Copyright (C) 2000, Yannick LE NY <y-le-ny@ifrance.com>
#    Copyright (C) 2000, Paul Fenwick <pjf@cpan.org>
#    Copyright (C) 2000, Brent Neal <brentn@users.sourceforge.net>
#    Copyright (C) 2013, Manoj Kumar
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
#    02111-1307, USA
#
# This code derived from Padzensky's work on package Finance::YahooQuote,
# but extends its capabilites to encompas a greater number of data sources.
#
# This code was developed as part of GnuCash <http://www.gnucash.org/>

package Finance::Quote::MaxLife;
require 5.005;

use strict;
use HTML::TreeBuilder;
use Finance::Quote::SymbolTranslation qw(symbol_translation);

our ($VERSION, $PLAN_URL, $NAV_URL, $DEBUG);

$VERSION = '1.18';
$DEBUG = 0;
$PLAN_URL = 'http://www.maxlifeinsurance.com/FundPerformance/fund_performance_result.aspx';
#$PLAN_URL = 'http://m.maxlifeinsurance.com/FundPerformance/fund_performance_result.aspx';
$NAV_URL = 'http://www.maxlifeinsurance.com/FundPerformance/ajax.aspx?RequestType=GetFundValuesNew&RequestValue=';

my (%plans, %fund_values, %plans_checked);

sub methods {
	return (
		max_life => \&max_life
	);
}

{
	my @labels = qw/currency date isodate method nav success symbol/;
	sub labels {
		return (
			max_life => \@labels
		);
	}
}

sub max_life {
	my $quoter = shift;
	my @symbols = @_;
	
	if (@symbols && $symbols[0] =~ /^Debug$|^Debug=(\d+)$/) { # if first symbol is Debug, enable debugging
    if (defined $1) {
      $DEBUG = $1;
    }
    else {
      $DEBUG = 1;
    }
    shift @symbols;
  }
  
	return unless @symbols;

	my %info;
	my $ua;
  my $symbol;
	my ($ref_trans_1);
	my ($plan_fund, $plan_name, $plan_num, $fund_name);
	
	$ua = $quoter->user_agent->clone();
  $ua->agent('Mozilla/5.0 (Windows NT 6.1; WOW64; rv:44.0) Gecko/20100101 Firefox/44.0');
  
	($ref_trans_1, undef) = symbol_translation('max_life', \@symbols, 1);
	
	foreach $symbol (@symbols) {
		$plan_fund = $ref_trans_1->{uc($symbol)};
		$plan_fund =~ s/_/ /g;
		if ($plan_fund =~ /(.+);(.+)/) {
      $plan_name = uc($1);
      $fund_name = uc($2);
    }
    else {
      $plan_name = '';
      $fund_name = uc($plan_fund);
    }
    
    if ($plan_name !~ /^\d+$/ && ! %plans) {
      get_plans($ua, $PLAN_URL, \%plans);
      if ($DEBUG & 4) {
        print "Plans:\n";
        foreach (sort keys %plans) {
          print "  $_ -> $plans{$_}\n";
        }
      }
    }
    
		$plan_num = $plans{$plan_name} // $plan_name;
		
		if (! exists $fund_values{$fund_name}) {
      if ($plan_num ne '' && ! exists $plans_checked{$plan_num}) {
        $plans_checked{$plan_num} = 1;
        get_plan_navs($ua, $NAV_URL, $plan_num, \%fund_values);
      }
      else {
        foreach (sort keys %plans) {
          next if (exists $plans_checked{$plans{$_}});
          $plans_checked{$plans{$_}} = 1;
          get_plan_navs($ua, $NAV_URL, $plans{$_}, \%fund_values);
          last if (exists $fund_values{$fund_name});
        }
      }
		}
		
		if (exists $fund_values{$fund_name} && @{$fund_values{$fund_name}} >= 2) {
			$quoter->store_date(\%info, $symbol, {eurodate => $fund_values{$fund_name}->[0]});
			$info{$symbol, 'nav'} = $fund_values{$fund_name}->[1];
			$info{$symbol, 'symbol'} = $fund_name;
			$info{$symbol, 'currency'} = 'INR';
			$info{$symbol, 'success'} = 1;
		}
		else {
			$info{$symbol, 'success'} = 0;
			$info{$symbol, 'errormsg'} = 'Stock lookup failed';
		}
		
		$info{$symbol, 'method'} = 'max_life';
	}
	
	return wantarray ? %info : \%info;
}


sub get_plans {
	my $ua = shift;
	my $url = shift;
	my $ref_plans = shift;
	my ($response, $element);
	
	if ($DEBUG & 1) {
		print "max_life: plan url: $url\n";
	}
	
	$response = $ua->get($url);
	
	if (! $response->is_success) {
		return;
	}
	
	$element = HTML::TreeBuilder->new_from_content($response->decoded_content());

	if ($element) {
    if ($DEBUG & 32) {
      print "Tree:\n";
      $element->dump();
      print "\n";
    }
		$element = $element->look_down('_tag' => 'select', 'id' => qr/NavProduct/);
	}
	
	if ($element) {
		foreach ($element->content_list()) {
			next unless ($_->tag() eq 'option' && defined $_->attr('value') && $_->as_text() !~ m/-+\s+.+\s+-+/);
			$ref_plans->{uc($_->as_text())} = $_->attr('value');
		}
	}
}


sub get_plan_navs {
	my $ua = shift;
	my $url = shift;
	my $plan_num = shift;
	my $ref_fund_values = shift;
	my ($response);
	
	$url .= $plan_num;
	
	if ($DEBUG & 1) {
		print "max_life: nav url: $url\n";
	}
	
	$response = $ua->get($url);
	
	if ($response->is_success) {
		my ($content, @data, $date1, @fund_values1, $date2, @fund_values2, @fund_idents, @fund_names);
		
		$content = $response->decoded_content();
    if ($DEBUG & 8) {
      print "Content: $content\n";
    }
    
		$content =~ s/.*\xA7//;
		@data = split(/\#/, $content);
    if (@data < 4) {
      return;
    }
    
    if ($DEBUG & 16) {
      print "Data:\n", join("\n", @data), "\n";
    }
    
		($date1, @fund_values1) = split(/!/, $data[0]); # date and fund navs for date n
		($date2, @fund_values2) = split(/!/, $data[1]); # date and fund navs for date n-1
                                                    # $data[2] - % change between two NAVs
		(undef, @fund_idents) = split(/\|/, $data[3]);  # fund names and SFIN
    if (@fund_values1 < 1 || @fund_values1 != @fund_values2 || @fund_values2 != @fund_idents) {
      return;
    }
    
		for (my $i = 0; $i < @fund_idents; $i++) {
			@fund_names = split(/!/, $fund_idents[$i]);
      next if (@fund_names < 3);
			if ($DEBUG & 2) {
				print "max_life: ", join(' : ', @fund_names), " : $date1, $fund_values1[$i], $date2, $fund_values2[$i]\n";
			}
			$ref_fund_values->{uc($fund_names[2])} = [$date1, $fund_values1[$i], $date2, $fund_values2[$i]];
		}
	}
}


1;

=head1 NAME

Finance::Quote::MaxLife  - Fetch quotes from Max Life Insurance web site http://www.maxlifeinsurance.com

=head1 SYNOPSIS

    use Finance::Quote;

    $q = Finance::Quote->new();

    %info = $q->fetch('max_life', '28;ULIF00425/06/04LIFESECURE104'); # <plan name|plan number>;<SFIN> 
    %info = $q->fetch('max_life', 'Max Life Shiksha Plus;ULIF00425/06/04LIFESECURE104'); # <plan name|plan number>;<SFIN> 
    %info = $q->fetch('max_life', 'FUND1'); # if FUND1 can be translated using Finance::Quote::SymbolTranslation to <plan name|plan number>;<SFIN>

=head1 DESCRIPTION

This module obtains information about Max Life Insurance funds from its web site http://www.maxlifeinsurance.com.

=head1 LABELS RETURNED

this source returns following lables on success:

	symbol - Max Life Insurance fund SFIN
	currency - INR
	nav - NAV in INR
	date - date in US (mm/dd/yyyy) format
	isodate - date in ISO (yyyy-mm-dd) format
	method - max_life
	success - 0 = failure / 1 = success
	errormsg - only if success is 0
	
=head1 SEE ALSO

Max Life Insurance web site http://www.maxlifeinsurance.com

Finance::Quote

=cut
