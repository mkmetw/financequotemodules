#
#    Copyright (C) 1998, Dj Padzensky <djpadz@padz.net>
#    Copyright (C) 1998, 1999 Linas Vepstas <linas@linas.org>
#    Copyright (C) 2000, Yannick LE NY <y-le-ny@ifrance.com>
#    Copyright (C) 2000, Paul Fenwick <pjf@cpan.org>
#    Copyright (C) 2000, Brent Neal <brentn@users.sourceforge.net>
#    Copyright (C) 2013, Manoj Kumar
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
#    02111-1307, USA
#
# This code derived from Padzensky's work on package Finance::YahooQuote,
# but extends its capabilites to encompas a greater number of data sources.
#
# This code was developed as part of GnuCash <http://www.gnucash.org/>

package Finance::Quote::Opcvm;
require 5.005;

use strict;
use warnings;
use HTML::TreeBuilder;

our ($VERSION, $MAIN_URL, $URL);

$VERSION = '1.18';

$MAIN_URL = 'http://geco.amf-france.org/';
$URL = $MAIN_URL . 'bio/res_part.aspx?CodeISIN=';

sub methods {
	return (
		opcvm => \&amf_opcvm
	);
}


{
	my @labels = qw/symbol currency source link name nav date isodate method/;
	sub labels {
		return (
			opcvm => \@labels
		);
	}
}

sub amf_opcvm {
	my $quoter = shift;
	my @symbols = @_;
    
	return unless @symbols;

	my (%info);
	my($ua, $url, $reply);

	$ua = $quoter->user_agent;

	foreach my $symbol (@symbols) {
		$info{$symbol, 'method'} = 'opcvm';
		$info{$symbol, 'success'} = 0;
		
		$url = $URL . $symbol;
		
		my $response = $ua->get($url);
		if ($response->is_success) {
			my $root = HTML::TreeBuilder->new();
			
			$root->utf8_mode(1); # avoid warning Parsing of undecoded UTF-8 will give garbage when decoding entities at
			
			$root->parse_content($response->content);
			my ($name, $nav, $date) = extract_opcvm_info($root);
			
			if (defined $name && defined $nav && defined $date) {
				$info{$symbol, 'symbol'} = $symbol;
				$info{$symbol, 'currency'} = 'EUR';
				$info{$symbol, 'source'} = $MAIN_URL;
				$info{$symbol, 'link'} = $url;
				
				$info{$symbol, 'name'} = $name;
				$info{$symbol, 'nav'} = $nav;
				$quoter->store_date(\%info, $symbol, {eurodate => $date});
				$info{$symbol, 'success'} = 1;
			}
			else {
				$info{$symbol, 'errormsg'} = 'OPCVM lookup failed';
			}
			
			$root->delete();
		}
		else {
			$info{$symbol, 'errormsg'} = 'OPCVM lookup failed';
		}
	}
	
	return wantarray ? %info : \%info;
}


sub extract_opcvm_info {
	my $root = shift;
	
	my ($table, $element);
	my ($prod, $prod_cat, $date, $value);

	$element = $root->look_down('_tag', 'td',
		sub {
			! $_[0]->look_down('_tag', 'table') && $_[0]->as_text() =~ m/^\s*Produit\s+:\s*$/;
		}
	);

	if ($element) {
		$element = $element->right();
	}
	
	if ($element) {
		if ($element->content_list() > 0) {
			$prod = ($element->content_list())[0]->as_text();
		}
		else {
			$prod = $element->as_text();
		}
		$prod =~ s/^\s+//;
		$prod =~ s/\s+$//;
	}

	$element = $root->look_down('_tag', 'td',
		sub {
			! $_[0]->look_down('_tag', 'table') && $_[0]->as_text() =~ m/^\s*Cat.+gorie part\s+:\s*$/;
		}
	);

	if ($element) {
		$element = $element->right();
	}
	
	if ($element && defined $prod) {
		my $prod_cat = $element->as_text();
		$prod_cat =~ s/^\s+//;
		$prod_cat =~ s/\s+$//;
		$prod .= " $prod_cat";
		$prod =~ s/\s+$//;
	}
	else {
		$prod = undef;
	}
	
	$element = $root->look_down('_tag', 'td',
		sub {
			! $_[0]->look_down('_tag', 'table') && $_[0]->as_text() =~ m/^\s*Date\s+VL\s+:\s*$/;
		}
	);

	if ($element) {
		$element = $element->right();
	}

	if ($element) {
		$date = $element->as_text();
		$date =~ s/^\s+//;
		$date =~ s/\s+$//;
	}
	
	$element = $root->look_down('_tag', 'td',
		sub {
			! $_[0]->look_down('_tag', 'table') && $_[0]->as_text() =~ m/^\s*Valeur\s+\(.+\)\s+:\s*$/;
		}
	);

	if ($element) {
		$element = $element->right();
	}

	if ($element) {
		$value = $element->as_text();
		$value =~ s/^\s+//;
		$value =~ s/\s+$//;
		$value =~ s/,/./
	}
	
	return($prod, $value, $date);
}

1;

=head1 NAME

Finance::Quote::Opcvm  - Obtain French mutual fund prices from http://www.amf-france.org

=head1 SYNOPSIS

    use Finance::Quote;

    $q = Finance::Quote->new();

    %stockinfo = $q->fetch('opcvm', @isin_codes);

=head1 DESCRIPTION

This module obtains information about French OPCVM (mutual fund) from the Autorit� des MarchE�s Financiers web site www.amf-france.org.

=head1 LABELS RETURNED

this source returns following lables on success:

	symbol - ISIN code
	currency - EUR
	source - http://www.amf-france.org/
	link - http://geco.amf-france.org/bio/res_part.aspx?CodeISIN=<symbol>
	name - fund name
	nav - NAV in Euro
	date - date in US (mm/dd/yyyy) format
	isodate - date in ISO (yyyy-mm-dd) format
	method - opcvm
	
=head1 SEE ALSO

Autorit� des March�s Financiers web site http://www.amf-france.org.

Finance::Quote

=cut
